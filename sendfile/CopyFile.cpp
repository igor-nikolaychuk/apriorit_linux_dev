#include <iostream>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <unistd.h>
#include <cerrno>
#include <cstring>

#include "CopyFile.h"

bool copyFile(const char* source, const char* dest) {
	int sourceFd;
	int destFd;

	sourceFd = open(source, O_RDONLY);
	if(sourceFd == -1) {
		std::cout << "Unable to open " << source << ": "
		<< std::strerror(errno) << std::endl;
		return false;
	}

	struct stat sourceStat;
	if(fstat(sourceFd, &sourceStat) == -1) {
		std::cout << "Unable to stat() source file: " 
			<< std::strerror(errno) << std::endl;
		close(sourceFd);
		return false;	
	}

	destFd = open(dest, O_CREAT | O_WRONLY | O_TRUNC, sourceStat.st_mode);
	if(destFd == -1) {
		std::cout << "Unable to open " << dest << ": "
		<< std::strerror(errno) << std::endl;
		close(sourceFd);
		return false;
	}

	if(sendfile(destFd, sourceFd, 0, sourceStat.st_size) == -1) {
		std::cout << "Unable to copy: " 
			  << std::strerror(errno) << std::endl;
		close(sourceFd);
		close(destFd);
		return false;
	}

	close(sourceFd);
	close(destFd);
	return true;
}
