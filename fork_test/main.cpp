#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <cerrno>
#include <cstring>

int main(int argc, char* argv[]) {
	if(argc != 2) {
		std::cout << "Usage: fork_test file_name" << std::endl;
		return 1;
	}

	int destFd = open(argv[1], O_CREAT | O_WRONLY | O_TRUNC, 0664);
	if(destFd == -1) {
		std::cout << "Unable to open: " << argv[1]
			<< strerror(errno) << std::endl;
		return false;
	};

	int pipes[2];
	pipe(pipes);

	pid_t pid = fork();

	if(pid) {
		//Parent
		const int buf_size = 256;
		char buf[buf_size];
		int len;
		
		close(pipes[1]);
		while ((len = read(pipes[0], buf, buf_size)) != 0) {
			write(destFd, buf, len);
		}

		char message[] = "Hello.";

		write(destFd, message, sizeof(message) - 1);
		close(pipes[0]);
		close(destFd);
	}
	else {
		//Child
		char message[] = "Hi daddy, I'm your child.";

		close(pipes[0]);
		write(pipes[1], message, sizeof(message) - 1);
		close(pipes[1]);
	}
}
